# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata.
# Omer I.S. <omeritzicschwartz@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-19 00:59+0000\n"
"PO-Revision-Date: 2020-10-16 09:44+0300\n"
"Last-Translator: Omer I.S. <omeritzicschwartz@gmail.com>\n"
"Language-Team: Hebrew <>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: discover/DiscoverObject.cpp:159
#, kde-format
msgid ""
"Discover currently cannot be used to install any apps or perform system "
"updates because none of its app backends are available."
msgstr ""

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can install some on the Settings page, under the <interface>Missing "
"Backends</interface> section.<nl/><nl/>Also please consider reporting this "
"as a packaging issue to your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:168 discover/DiscoverObject.cpp:380
#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgid "Report This Issue"
msgstr ""

#: discover/DiscoverObject.cpp:173
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can use <command>pacman</command> to install the optional dependencies "
"that are needed to enable the application backends.<nl/><nl/>Please note "
"that Arch Linux developers recommend using <command>pacman</command> for "
"managing software because the PackageKit backend is not well-integrated on "
"Arch Linux."
msgstr ""

#: discover/DiscoverObject.cpp:181
#, kde-format
msgid "Learn More"
msgstr ""

#: discover/DiscoverObject.cpp:269
#, kde-format
msgid "Could not find category '%1'"
msgstr "לא היה ניתן למצוא קטגוריית \"%1\""

#: discover/DiscoverObject.cpp:284
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"לא ניתן לתקשר עם משאבי flatpak ללא הגישור החבוי %1 ל־flatpak. נא להתקינו "
"קודם."

#: discover/DiscoverObject.cpp:310
#, fuzzy, kde-format
#| msgid "Couldn't open %1"
msgid "Could not open %1"
msgstr "לא היה ניתן לפתוח את %1"

#: discover/DiscoverObject.cpp:372
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr ""

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:442 discover/DiscoverObject.cpp:443
#: discover/main.cpp:120 discover/qml/BrowsingPage.qml:20
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:443
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""

#: discover/main.cpp:42
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr ""

#: discover/main.cpp:43
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr ""

#: discover/main.cpp:44
#, kde-format
msgid "Display a list of entries with a category."
msgstr "הצגת רשימה של ערכים עם קטגוריה."

#: discover/main.cpp:45
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""

#: discover/main.cpp:46
#, kde-format
msgid "List all the available modes."
msgstr "רישום כל המצבים הזמינים."

#: discover/main.cpp:47
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr "מצב קומפקטי (אוטומטי/קומפקטי/מלא)."

#: discover/main.cpp:48
#, kde-format
msgid "Local package file to install"
msgstr "חבילה מקומית להתקנה"

#: discover/main.cpp:49
#, kde-format
msgid "List all the available backends."
msgstr "פירוט כל הגישורים החבויים הזמינים."

#: discover/main.cpp:50
#, kde-format
msgid "Search string."
msgstr "חיפוש מחרוזת."

#: discover/main.cpp:51
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "פירוט כל האפשרויות הזמינות למשוב משתמש"

#: discover/main.cpp:53
#, kde-format
msgid "Supports appstream: url scheme"
msgstr ""

#: discover/main.cpp:122
#, kde-format
msgid "An application explorer"
msgstr "סייר יישומים"

#: discover/main.cpp:124
#, fuzzy, kde-format
#| msgid "© 2010-2019 Plasma Development Team"
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2019 צוות פיתוח Plasma"

#: discover/main.cpp:125
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr ""

#: discover/main.cpp:126
#, kde-format
msgid "Nate Graham"
msgstr ""

#: discover/main.cpp:127
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr ""

#: discover/main.cpp:131
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr ""

#: discover/main.cpp:132
#, kde-format
msgid "KNewStuff"
msgstr ""

#: discover/main.cpp:139
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "צוות התרגום של KDE ישראל,,עומר א״ש"

#: discover/main.cpp:139
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-l10n-he@kde.org,,omeritzicschwartz@gmail.com"

#: discover/main.cpp:152
#, kde-format
msgid "Available backends:\n"
msgstr "הגישורים החבויים הזמינים:\n"

#: discover/main.cpp:208
#, kde-format
msgid "Available modes:\n"
msgstr "המצבים הזמינים:\n"

#: discover/qml/AddonsView.qml:19 discover/qml/navigation.js:43
#, fuzzy, kde-format
#| msgid "Addons"
msgid "Addons for %1"
msgstr "תוספים"

#: discover/qml/AddonsView.qml:51
#, kde-format
msgid "More…"
msgstr ""

#: discover/qml/AddonsView.qml:60
#, kde-format
msgid "Apply Changes"
msgstr "החלת הגדרות"

#: discover/qml/AddonsView.qml:67
#, kde-format
msgid "Reset"
msgstr ""

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr "הוספת מאגר %1 חדש"

#: discover/qml/AddSourceDialog.qml:44
#, kde-format
msgid "Add"
msgstr "הוספה"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:269
#: discover/qml/InstallApplicationButton.qml:43
#: discover/qml/ProgressView.qml:109 discover/qml/SourcesPage.qml:190
#: discover/qml/UpdatesPage.qml:252 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "ביטול"

#: discover/qml/ApplicationDelegate.qml:141
#: discover/qml/ApplicationPage.qml:209
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "דירוג %1"
msgstr[1] "%1 דירוגים"

#: discover/qml/ApplicationDelegate.qml:141
#: discover/qml/ApplicationPage.qml:209
#, kde-format
msgid "No ratings yet"
msgstr "אין דירוגים עדיין"

#: discover/qml/ApplicationPage.qml:63
#, kde-format
msgid "Sources"
msgstr "מקורות"

#: discover/qml/ApplicationPage.qml:74
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:186
#, kde-format
msgid "Unknown author"
msgstr ""

#: discover/qml/ApplicationPage.qml:221
#, kde-format
msgid "Could not access the screenshots"
msgstr ""

#: discover/qml/ApplicationPage.qml:280
#, fuzzy, kde-format
#| msgid "Version:"
msgid "Version"
msgstr "גרסה:"

#: discover/qml/ApplicationPage.qml:307
#: discover/qml/ApplicationsListPage.qml:110
#, kde-format
msgid "Size"
msgstr "גודל"

#: discover/qml/ApplicationPage.qml:334
#, kde-format
msgid "Distributed by"
msgstr ""

#: discover/qml/ApplicationPage.qml:361
#, fuzzy, kde-format
#| msgid "License:"
msgid "License"
msgid_plural "Licenses"
msgstr[0] "רישיון:"
msgstr[1] "רישיון:"

#: discover/qml/ApplicationPage.qml:370
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr ""

#: discover/qml/ApplicationPage.qml:408
#, fuzzy, kde-format
#| msgid "What's New"
msgid "What does this mean?"
msgstr "מה חדש"

#: discover/qml/ApplicationPage.qml:418
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] ""
msgstr[1] ""

#: discover/qml/ApplicationPage.qml:436 discover/qml/ApplicationPage.qml:885
#, fuzzy, kde-format
#| msgid "Enter a rating"
msgid "Content Rating"
msgstr "נא להזין דירוג"

#: discover/qml/ApplicationPage.qml:446
#, kde-format
msgid "Age: %1+"
msgstr ""

#: discover/qml/ApplicationPage.qml:470
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr ""

#: discover/qml/ApplicationPage.qml:569
#, fuzzy, kde-format
#| msgid "Documentation:"
msgid "Documentation"
msgstr "תיעוד:"

#: discover/qml/ApplicationPage.qml:570
#, kde-format
msgid "Read the project's official documentation"
msgstr ""

#: discover/qml/ApplicationPage.qml:586
#, kde-format
msgid "Website"
msgstr ""

#: discover/qml/ApplicationPage.qml:587
#, fuzzy, kde-format
#| msgid "Visit the app's website"
msgid "Visit the project's website"
msgstr "ביקור באתר של היישום"

#: discover/qml/ApplicationPage.qml:603
#, kde-format
msgid "Addons"
msgstr "תוספים"

#: discover/qml/ApplicationPage.qml:604
#, kde-format
msgid "Install or remove additional functionality"
msgstr ""

#: discover/qml/ApplicationPage.qml:623
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr ""

#: discover/qml/ApplicationPage.qml:624
#, kde-format
msgid "Send a link for this application"
msgstr ""

#: discover/qml/ApplicationPage.qml:640
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr ""

#: discover/qml/ApplicationPage.qml:660
#, kde-format
msgid "What's New"
msgstr "מה חדש"

#: discover/qml/ApplicationPage.qml:691
#, fuzzy, kde-format
#| msgid "Reviews for %1"
msgid "Loading reviews for %1"
msgstr "ביקורות על %1"

#: discover/qml/ApplicationPage.qml:697
#, kde-format
msgid "Reviews"
msgstr "ביקורות"

#: discover/qml/ApplicationPage.qml:735
#, fuzzy, kde-format
#| msgid "Show %1 review..."
#| msgid_plural "Show all %1 reviews..."
msgid "Show all %1 Reviews"
msgid_plural "Show all %1 Reviews"
msgstr[0] "הצגת ביקורת %1..."
msgstr[1] "הצגת %1 ביקורות..."

#: discover/qml/ApplicationPage.qml:747
#, fuzzy, kde-format
#| msgid "Write a Review..."
msgid "Write a Review"
msgstr "כתיבת ביקורת..."

#: discover/qml/ApplicationPage.qml:747
#, fuzzy, kde-format
#| msgid "Install to write a review!"
msgid "Install to Write a Review"
msgstr "יש להתקין כדי לכתוב ביקורת!"

#: discover/qml/ApplicationPage.qml:759
#, fuzzy, kde-format
#| msgid "Get involved:"
msgid "Get Involved"
msgstr "לקיחת חלק:"

#: discover/qml/ApplicationPage.qml:792
#, kde-format
msgid "Donate"
msgstr ""

#: discover/qml/ApplicationPage.qml:793
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""

#: discover/qml/ApplicationPage.qml:809
#, kde-format
msgid "Report Bug"
msgstr ""

#: discover/qml/ApplicationPage.qml:810
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr ""

#: discover/qml/ApplicationPage.qml:826
#, fuzzy, kde-format
#| msgid "Contribute..."
msgid "Contribute"
msgstr "תרומה..."

#: discover/qml/ApplicationPage.qml:827
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr ""

#: discover/qml/ApplicationPage.qml:852
#, fuzzy, kde-format
#| msgid "License:"
msgid "All Licenses"
msgstr "רישיון:"

#: discover/qml/ApplicationPage.qml:899
#, kde-format
msgid "Risks of proprietary software"
msgstr ""

#: discover/qml/ApplicationPage.qml:905
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""

#: discover/qml/ApplicationPage.qml:906
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:54
#, fuzzy, kde-format
#| msgid "Search: %1"
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "חיפוש: %1"
msgstr[1] "חיפוש: %1"

#: discover/qml/ApplicationsListPage.qml:56
#, kde-format
msgid "Search: %1"
msgstr "חיפוש: %1"

#: discover/qml/ApplicationsListPage.qml:60
#, fuzzy, kde-format
#| msgid "%1 - %2"
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%1 - %2"
msgstr[1] "%1 - %2"

#: discover/qml/ApplicationsListPage.qml:66
#, fuzzy, kde-format
#| msgid "Search: %1"
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "חיפוש: %1"
msgstr[1] "חיפוש: %1"

#: discover/qml/ApplicationsListPage.qml:68
#: discover/qml/ApplicationsListPage.qml:226
#, kde-format
msgid "Search"
msgstr "חיפוש"

#: discover/qml/ApplicationsListPage.qml:89
#, kde-format
msgid "Sort: %1"
msgstr "מיון לפי: %1"

#: discover/qml/ApplicationsListPage.qml:92
#, kde-format
msgid "Name"
msgstr "שם"

#: discover/qml/ApplicationsListPage.qml:101
#, kde-format
msgid "Rating"
msgstr "דירוג"

#: discover/qml/ApplicationsListPage.qml:119
#, kde-format
msgid "Release Date"
msgstr "תאריך שחרור"

#: discover/qml/ApplicationsListPage.qml:174
#, fuzzy, kde-format
#| msgid "Sorry, nothing found"
msgid "Nothing found"
msgstr "לא נמצא דבר, עמך הסליחה"

#: discover/qml/ApplicationsListPage.qml:181
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:190
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr ""

#: discover/qml/ApplicationsListPage.qml:194
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:205
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:207
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:208
#, kde-format
msgctxt "@info:placeholder%1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:241
#, fuzzy, kde-format
#| msgid "Still looking..."
msgid "Still looking…"
msgstr "עדיין בחיפוש..."

#: discover/qml/BrowsingPage.qml:50
#, kde-format
msgid "Unable to load applications"
msgstr ""

#: discover/qml/BrowsingPage.qml:89
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr ""

#: discover/qml/BrowsingPage.qml:107
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr ""

#: discover/qml/BrowsingPage.qml:121
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr ""

#: discover/qml/BrowsingPage.qml:140 discover/qml/BrowsingPage.qml:169
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr ""

#: discover/qml/BrowsingPage.qml:150
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr ""

#: discover/qml/DiscoverWindow.qml:44
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "הרצה בתור <em>משתמש־על (root)</em> היא מסוכנת ומיותרת."

#: discover/qml/DiscoverWindow.qml:57
#, fuzzy, kde-format
msgid "&Home"
msgstr "דך הבית"

#: discover/qml/DiscoverWindow.qml:67
#, fuzzy, kde-format
#| msgid "Search"
msgid "&Search"
msgstr "חיפוש"

#: discover/qml/DiscoverWindow.qml:75
#, fuzzy, kde-format
#| msgid "Installed"
msgid "&Installed"
msgstr "מותקן"

#: discover/qml/DiscoverWindow.qml:87
#, fuzzy, kde-format
#| msgid "Fetching updates..."
msgid "Fetching &updates…"
msgstr "באחזור העדכונים..."

#: discover/qml/DiscoverWindow.qml:87
#, fuzzy, kde-format
#| msgid "Up to date"
msgid "&Up to date"
msgstr "המערכת מעודכנת"

#: discover/qml/DiscoverWindow.qml:88
#, fuzzy, kde-format
#| msgctxt "Update section name"
#| msgid "Update (%1)"
msgctxt "Update section name"
msgid "&Update (%1)"
msgstr "עדכון (%1)"

#: discover/qml/DiscoverWindow.qml:96
#, fuzzy, kde-format
#| msgid "About"
msgid "&About"
msgstr "אודות"

#: discover/qml/DiscoverWindow.qml:104
#, fuzzy, kde-format
#| msgid "Settings"
msgid "S&ettings"
msgstr "הגדרות"

#: discover/qml/DiscoverWindow.qml:157 discover/qml/DiscoverWindow.qml:338
#: discover/qml/DiscoverWindow.qml:445
#, kde-format
msgid "Error"
msgstr ""

#: discover/qml/DiscoverWindow.qml:161
#, kde-format
msgid "Unable to find resource: %1"
msgstr "אין יכולת למצוא את המשאב: %1"

#: discover/qml/DiscoverWindow.qml:256 discover/qml/SourcesPage.qml:180
#, kde-format
msgid "Proceed"
msgstr "המשך"

#: discover/qml/DiscoverWindow.qml:314
#, kde-format
msgid "Report this issue"
msgstr ""

#: discover/qml/DiscoverWindow.qml:338
#, fuzzy, kde-format
#| msgid "Votes: %1 out of %2"
msgid "Error %1 of %2"
msgstr "הצבעות: %1 מתוך %2"

#: discover/qml/DiscoverWindow.qml:383
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr ""

#: discover/qml/DiscoverWindow.qml:396
#, fuzzy, kde-format
#| msgid "Show contents"
msgctxt "@action:button"
msgid "Show Next"
msgstr "הצגת תוכן"

#: discover/qml/DiscoverWindow.qml:412 discover/qml/UpdatesPage.qml:98
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: discover/qml/Feedback.qml:13
#, kde-format
msgid "Submit usage information"
msgstr "שליחת פרטי שימוש"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"שליחת פרטי שימוש ל־KDE בעילום שם כדי שנוכל להבין את המשתמשים שלנו טוב יותר. "
"מידע נוסף בכתובת https://kde.org/privacypolicy-apps.php."

#: discover/qml/Feedback.qml:18
#, fuzzy, kde-format
#| msgid "Submitting usage information..."
msgid "Submitting usage information…"
msgstr "בשליחת פרטי שימוש..."

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Configure"
msgstr "הגדרה"

#: discover/qml/Feedback.qml:22
#, fuzzy, kde-format
#| msgid "Configure feedback..."
msgid "Configure feedback…"
msgstr "הגדרת משוב..."

#: discover/qml/Feedback.qml:29 discover/qml/SourcesPage.qml:21
#, fuzzy, kde-format
#| msgid "Configure feedback..."
msgid "Configure Updates…"
msgstr "הגדרת משוב..."

#: discover/qml/Feedback.qml:57
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr "ניתן לעזור לנו לשפר את היישום הזה בשיתוף הסטטיסטיקה והשתתפות בסקרים."

#: discover/qml/Feedback.qml:57
#, fuzzy, kde-format
#| msgid "Contribute..."
msgid "Contribute…"
msgstr "תרומה..."

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "We are looking for your feedback!"
msgstr "אנחנו מחכים למשוב שלך!"

#: discover/qml/Feedback.qml:62
#, fuzzy, kde-format
#| msgid "Participate..."
msgid "Participate…"
msgstr "השתתפות..."

#: discover/qml/InstallApplicationButton.qml:25
#, fuzzy, kde-format
#| msgid "Loading..."
msgctxt "State being fetched"
msgid "Loading…"
msgstr "בטעינה..."

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgid "Install"
msgstr "התקנה"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgid "Remove"
msgstr "הסרה"

#: discover/qml/InstalledPage.qml:16
#, kde-format
msgid "Installed"
msgstr "מותקן"

#: discover/qml/navigation.js:18
#, kde-format
msgid "Resources for '%1'"
msgstr ""

#: discover/qml/ProgressView.qml:17
#, kde-format
msgid "Tasks (%1%)"
msgstr "משימות (%1%)"

#: discover/qml/ProgressView.qml:17 discover/qml/ProgressView.qml:42
#, kde-format
msgid "Tasks"
msgstr "משימות"

#: discover/qml/ProgressView.qml:102
#, fuzzy, kde-format
#| msgctxt "TransactioName - TransactionStatus"
#| msgid "%1 - %2: %3"
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:103
#, fuzzy, kde-format
#| msgctxt "TransactioName - TransactionStatus"
#| msgid "%1 - %2: %3"
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:104
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:64
#, kde-format
msgid "unknown reviewer"
msgstr "נותן/ת ביקורת לא ידוע/ה"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> מאת %2"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "Comment by %1"
msgstr "הערה מאת %1"

#: discover/qml/ReviewDelegate.qml:83
#, kde-format
msgid "Version: %1"
msgstr "גרסה: %1"

#: discover/qml/ReviewDelegate.qml:83
#, fuzzy, kde-format
#| msgid "Version: %1"
msgid "Version: unknown"
msgstr "גרסה: %1"

#: discover/qml/ReviewDelegate.qml:98
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "הצבעות: %1 מתוך %2"

#: discover/qml/ReviewDelegate.qml:105
#, kde-format
msgid "Was this review useful?"
msgstr ""

#: discover/qml/ReviewDelegate.qml:117
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "כן"

#: discover/qml/ReviewDelegate.qml:134
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "לא"

#: discover/qml/ReviewDialog.qml:19
#, kde-format
msgid "Reviewing %1"
msgstr "בביקורת על %1"

#: discover/qml/ReviewDialog.qml:25
#, kde-format
msgid "Submit review"
msgstr "שליחת ביקורת"

#: discover/qml/ReviewDialog.qml:38
#, kde-format
msgid "Rating:"
msgstr "דירוג:"

#: discover/qml/ReviewDialog.qml:43
#, kde-format
msgid "Name:"
msgstr "שם:"

#: discover/qml/ReviewDialog.qml:51
#, kde-format
msgid "Title:"
msgstr "כותרת:"

#: discover/qml/ReviewDialog.qml:69
#, kde-format
msgid "Enter a rating"
msgstr "נא להזין דירוג"

#: discover/qml/ReviewDialog.qml:72
#, kde-format
msgid "Write the title"
msgstr "נא לכתוב את הכותרת"

#: discover/qml/ReviewDialog.qml:75
#, kde-format
msgid "Write the review"
msgstr "נא לכתוב את הביקורת"

#: discover/qml/ReviewDialog.qml:78
#, fuzzy, kde-format
#| msgid "Keep writing..."
msgid "Keep writing…"
msgstr "להמשיך לכתוב..."

#: discover/qml/ReviewDialog.qml:81
#, kde-format
msgid "Too long!"
msgstr "ארוך מדי!"

#: discover/qml/ReviewDialog.qml:84
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr ""

#: discover/qml/ReviewsPage.qml:44
#, kde-format
msgid "Reviews for %1"
msgstr "ביקורות על %1"

#: discover/qml/ReviewsPage.qml:55
#, fuzzy, kde-format
#| msgid "Write a Review..."
msgid "Write a Review…"
msgstr "כתיבת ביקורת..."

#: discover/qml/ReviewsPage.qml:60
#, kde-format
msgid "Install this app to write a review"
msgstr "יש להתקין יישום זה לכתיבת ביקורת"

#: discover/qml/SearchField.qml:23
#, fuzzy, kde-format
#| msgid "Search"
msgid "Search…"
msgstr "חיפוש"

#: discover/qml/SearchField.qml:23
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search in '%1'…"
msgstr "חיפוש בתוך \"%1\"..."

#: discover/qml/SourcesPage.qml:17
#, kde-format
msgid "Settings"
msgstr "הגדרות"

#: discover/qml/SourcesPage.qml:98
#, kde-format
msgid "Default source"
msgstr ""

#: discover/qml/SourcesPage.qml:105
#, fuzzy, kde-format
#| msgid "Add Source..."
msgid "Add Source…"
msgstr "הוספת מקור..."

#: discover/qml/SourcesPage.qml:131
#, kde-format
msgid "Make default"
msgstr "הפיכה לברירת המחדל"

#: discover/qml/SourcesPage.qml:222
#, kde-format
msgid "Increase priority"
msgstr ""

#: discover/qml/SourcesPage.qml:228
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "ההגדלה של העדפת \"%1\" נכשלה"

#: discover/qml/SourcesPage.qml:234
#, kde-format
msgid "Decrease priority"
msgstr ""

#: discover/qml/SourcesPage.qml:240
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "ההקטנה של העדפת \"%1\" נכשלה"

#: discover/qml/SourcesPage.qml:246
#, kde-format
msgid "Remove repository"
msgstr ""

#: discover/qml/SourcesPage.qml:257
#, kde-format
msgid "Show contents"
msgstr "הצגת תוכן"

#: discover/qml/SourcesPage.qml:296
#, kde-format
msgid "Missing Backends"
msgstr "גישורים חבויים שחסרים"

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "עדכונים"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Update Issue"
msgstr "בעיה בעדכון"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr ""

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr ""

#: discover/qml/UpdatesPage.qml:83
#, kde-format
msgid ""
"If you would like to report the update issue to your distribution's "
"packagers, include this information:"
msgstr ""

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Error message copied to clipboard"
msgstr ""

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update Selected"
msgstr "עדכון הנבחרים"

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update All"
msgstr "עדכון הכל"

#: discover/qml/UpdatesPage.qml:174
#, kde-format
msgid "Ignore"
msgstr ""

#: discover/qml/UpdatesPage.qml:220
#, kde-format
msgid "Select All"
msgstr ""

#: discover/qml/UpdatesPage.qml:228
#, kde-format
msgid "Select None"
msgstr ""

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Restart automatically after update has completed"
msgstr ""

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Total size: %1"
msgstr ""

#: discover/qml/UpdatesPage.qml:277
#, fuzzy, kde-format
#| msgid "Restart"
msgid "Restart Now"
msgstr "הפעלה מחדש"

#: discover/qml/UpdatesPage.qml:376
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:392
#, kde-format
msgid "Installing"
msgstr "בהתקנה"

#: discover/qml/UpdatesPage.qml:428
#, fuzzy, kde-format
#| msgid "Update All"
msgid "Update from:"
msgstr "עדכון הכל"

#: discover/qml/UpdatesPage.qml:440
#, fuzzy, kde-format
#| msgid "%1 (%2)"
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:447
#, fuzzy, kde-format
#| msgid "More Information..."
msgid "More Information…"
msgstr "מידע נוסף..."

#: discover/qml/UpdatesPage.qml:475
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "Fetching updates..."
msgctxt "@info"
msgid "Fetching updates…"
msgstr "באחזור העדכונים..."

#: discover/qml/UpdatesPage.qml:488
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "עדכונים"

#: discover/qml/UpdatesPage.qml:498
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr ""

#: discover/qml/UpdatesPage.qml:510 discover/qml/UpdatesPage.qml:517
#: discover/qml/UpdatesPage.qml:524
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "המערכת מעודכנת"

#: discover/qml/UpdatesPage.qml:531
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "כדאי לבדוק אם יש עדכונים"

#: discover/qml/UpdatesPage.qml:538
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr ""

#~ msgid "Featured"
#~ msgstr "מוצגים"

#, fuzzy
#~ msgid "Updates are available"
#~ msgstr "אין עדכונים זמינים."

#, fuzzy
#~| msgid "Show all"
#~ msgctxt "Short for 'show updates'"
#~ msgid "Show"
#~ msgstr "הצגת הכל"

#, fuzzy
#~| msgid "More..."
#~ msgid "Show more..."
#~ msgstr "עוד..."

#, fuzzy
#~| msgid "Could not close Discover, there are tasks that need to be done."
#~ msgid "Could not close Discover because some tasks are still in progress."
#~ msgstr "לא היה ניתן לסגור את Discover, ישנן משימות שלא הושלמו."

#~ msgid "Quit Anyway"
#~ msgstr "יציאה בכל זאת"

#, fuzzy
#~| msgid "Loading..."
#~ msgid "Loading…"
#~ msgstr "בטעינה..."

#~ msgid "Return to the Featured page"
#~ msgstr "חזרה אל עמוד המוצגים"

#~ msgid "Category:"
#~ msgstr "קטגוריה:"

#~ msgid "Author:"
#~ msgstr "מחבר/ת:"

#~ msgid "Size:"
#~ msgstr "גודל:"

#~ msgid "Source:"
#~ msgstr "מקור:"

#~ msgid "See full license terms"
#~ msgstr "הצגת תנאי הרישיון המלאים"

#~ msgid "Read the user guide"
#~ msgstr "קריאת המדריך למשתמש"

#~ msgid "Make a donation"
#~ msgstr "לתרום תרומה"

#~ msgid "Report a problem"
#~ msgstr "דיווח על בעיה"

#~ msgid "%1 (Default)"
#~ msgstr "%1 (ברירת המחדל)"

#, fuzzy
#~| msgid "Extensions..."
#~ msgid "Extensions…"
#~ msgstr "הרחבות..."

#, fuzzy
#~| msgctxt "@info"
#~| msgid ""
#~| "Unable to load applications.<nl/>Please verify Internet connectivity."
#~ msgid "Please verify Internet connectivity"
#~ msgstr "אין יכולת לטעון את היישומים.<nl/>נא לבדוק את החיבור שלך לאינטרנט."

#~ msgid "All updates selected (%1)"
#~ msgstr "בחירת כל העדכונים (%1)"

#~ msgid "%1/%2 update selected (%3)"
#~ msgid_plural "%1/%2 updates selected (%3)"
#~ msgstr[0] "עדכון %1/%2 נבחר (%3)"
#~ msgstr[1] "%1/%2 עדכונים נבחרו (%3)"

#~ msgid "OK"
#~ msgstr "אישור"

#~ msgctxt "Part of a string like this: '<app name> - <category>'"
#~ msgid "- %1"
#~ msgstr "- %1"

#, fuzzy
#~| msgid "Install"
#~ msgctxt "Install the version of an app that comes from Snap, Flatpak, etc"
#~ msgid "Install from %1"
#~ msgstr "התקנה"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "The system requires a restart to apply updates"
#~ msgctxt "@info"
#~ msgid "The system must be restarted to fully apply the installed updates"
#~ msgstr "הפעלת המערכת מחדש נדרשת להחלת העדכונים"

#~ msgid "%1, released on %2"
#~ msgstr "%1, שוחררה ב־%2"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "State being fetched"
#~ msgid "Loading..."
#~ msgstr "בטעינה..."

#~ msgid "Write a Review..."
#~ msgstr "כתיבת ביקורת..."

#~ msgid "Search..."
#~ msgstr "חיפוש..."

#~ msgctxt "@info"
#~ msgid "It is unknown when the last check for updates was"
#~ msgstr "מועד הבדיקה האחרונה אם יש עדכונים אינו ידוע"

#~ msgid "No application back-ends found, please report to your distribution."
#~ msgstr "לא נמצאו גישורים חבויים ליישומים, נא לדווח על כך להפצה שלך."

#~ msgid "Sorry..."
#~ msgstr "עמך הסליחה..."

#~ msgid "Useful?"
#~ msgstr "שימושי?"

#~ msgid "Update to version %1"
#~ msgstr "עדכון לגרסה %1"

#~ msgctxt "Do not translate or alter \\x9C"
#~ msgid "%1 → %2%1 → %2%2"
#~ msgstr "%1 → %2%1 → %2%2"

#~ msgid "Jonathan Thomas"
#~ msgstr "ג’ונתן תומאס"

#~ msgid "Discard"
#~ msgstr "ביטול"

#~ msgid "Write a review!"
#~ msgstr "ניתן לכתוב ביקורת!"

#~ msgid "Be the first to write a review!"
#~ msgstr "ניתן להיות הראשון/ה לכתוב ביקורת!"

#~ msgid "Install and be the first to write a review!"
#~ msgstr "יש להתקין כדי להיות הראשון/ה לכתוב ביקורת!"

#~ msgid "Hide"
#~ msgstr "הסתרה"

#~ msgid "Delete the origin"
#~ msgstr "מחיקת המקור"

#, fuzzy
#~ msgid "Homepage:"
#~ msgstr "דך הבית"

#, fuzzy
#~ msgid "Fetching Updates..."
#~ msgstr "אין עדכונים"

#, fuzzy
#~ msgctxt "@info"
#~ msgid "Fetching Updates..."
#~ msgstr "אין עדכונים"

#, fuzzy
#~| msgid "Review"
#~ msgid "Review:"
#~ msgstr "סקירה"

#, fuzzy
#~ msgid "Checking for updates..."
#~ msgstr "אין עדכונים"

#~ msgid "No Updates"
#~ msgstr "אין עדכונים"

#, fuzzy
#~ msgctxt "@info"
#~ msgid "Fetching..."
#~ msgstr "טוען..."

#, fuzzy
#~ msgctxt "@info"
#~ msgid "Checking for updates..."
#~ msgstr "אין עדכונים"

#, fuzzy
#~ msgctxt "@info"
#~ msgid "Updating..."
#~ msgstr "טוען..."

#, fuzzy
#~ msgctxt "@info"
#~ msgid "No updates"
#~ msgstr "אין עדכונים"

#~ msgid "updates not selected"
#~ msgstr "עדכן אלו שלא נבחרו"

#, fuzzy
#~ msgctxt "@info"
#~ msgid "Looking for updates"
#~ msgstr "אין עדכונים"

#~ msgid "Summary:"
#~ msgstr "תקציר:"

#~ msgid "Launch"
#~ msgstr "פתח"

#~ msgid "Description"
#~ msgstr "תיאור"

#~ msgid "Open"
#~ msgstr "פתח"

#, fuzzy
#~ msgid "Close Description"
#~ msgstr "תיאור"

#~ msgid "Got it"
#~ msgstr "קבל את זה"

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgid "Origin: %1 (%2)"
#~ msgstr "%1 (%2)"
