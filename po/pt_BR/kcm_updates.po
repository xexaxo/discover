# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-07 00:45+0000\n"
"PO-Revision-Date: 2022-10-19 15:08-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: kcm/package/contents/ui/main.qml:32
#, kde-format
msgid "Update software:"
msgstr "Atualizar aplicativo:"

#: kcm/package/contents/ui/main.qml:33
#, kde-format
msgid "Manually"
msgstr "Manually"

#: kcm/package/contents/ui/main.qml:43
#, kde-format
msgid "Automatically"
msgstr "Automaticamente"

#: kcm/package/contents/ui/main.qml:50
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Software updates will be downloaded automatically when they become "
"available. Updates for applications will be installed immediately, while "
"system updates will be installed the next time the computer is restarted."
msgstr ""
"As atualizações de aplicativos serão baixadas automaticamente quando ficarem "
"disponíveis. As atualizações para aplicativos serão instaladas imediatamente "
"enquanto que atualizações do sistema serão instaladas na próxima vez que o "
"computador for reiniciado."

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Update frequency:"
msgstr "Frequência de atualização:"

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Notification frequency:"
msgstr "Frequência de notificação:"

#: kcm/package/contents/ui/main.qml:64
#, kde-format
msgctxt "@item:inlistbox"
msgid "Daily"
msgstr "Diariamente"

#: kcm/package/contents/ui/main.qml:65
#, kde-format
msgctxt "@item:inlistbox"
msgid "Weekly"
msgstr "Semanalmente"

#: kcm/package/contents/ui/main.qml:66
#, kde-format
msgctxt "@item:inlistbox"
msgid "Monthly"
msgstr "Mensalmente"

#: kcm/package/contents/ui/main.qml:67
#, kde-format
msgctxt "@item:inlistbox"
msgid "Never"
msgstr "Nunca"

#: kcm/package/contents/ui/main.qml:111
#, kde-format
msgid "Use offline updates:"
msgstr "Usar atualizações offline:"

#: kcm/package/contents/ui/main.qml:124
#, kde-format
msgid ""
"Offline updates maximize system stability by applying changes while "
"restarting the system. Using this update mode is strongly recommended."
msgstr ""
"As atualizações offline maximizam a estabilidade do sistema ao aplicar as "
"alterações enquanto o sistema é reiniciado. Usar este modo de atualização é "
"altamente recomendado."

#: kcm/updates.cpp:31
#, kde-format
msgid "Software Update"
msgstr "Atualização de aplicativos"

#: kcm/updates.cpp:33
#, kde-format
msgid "Configure software update settings"
msgstr "Configurar as opções das atualizações de aplicativos"
